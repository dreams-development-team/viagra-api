import * as dotenv from "dotenv";
import * as fs from "fs";
import * as path from "path";

const DEFAULT_PORT = 3000;
const DEFAULT_ENV = "development";
const DEFAULT_HOST = "127.0.0.1";
const DEFAULT_JWT_SECRET = "jwt-secret-key";

const ENV = process.env.NODE_ENV || DEFAULT_ENV;

const IS_DEV = ENV === "development";
const IS_PROD = ENV === "production";
const IS_TEST = ENV === "test";

{
  const defaultEnvFilePath = path.join(__dirname, "../.env");
  const specificEnvFilePath = path.join(__dirname, `../.env.${ENV}`);
  const specificEnvFileExists = fs.existsSync(specificEnvFilePath);
  const envFilePath = specificEnvFileExists
    ? specificEnvFilePath
    : defaultEnvFilePath;

  dotenv.config({ path: envFilePath });
}

const PORT = Number(process.env.PORT) || DEFAULT_PORT;
const HOST = process.env.HOST || DEFAULT_HOST;
const PG_URL = process.env.PG_URL;
const JWT_SECRET = process.env.JWT_SECRET || DEFAULT_JWT_SECRET;
const CLOUD_API_KEY = process.env.CLOUD_API_KEY;
const CLOUD_NAME = process.env.CLOUD_NAME;
const CLOUD_SECRET = process.env.CLOUD_SECRET;

export {
  ENV,
  PORT,
  HOST,
  PG_URL,
  IS_DEV,
  IS_PROD,
  IS_TEST,
  JWT_SECRET,
  CLOUD_API_KEY,
  CLOUD_NAME,
  CLOUD_SECRET
};
