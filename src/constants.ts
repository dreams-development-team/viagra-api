export enum DELIVERY_TYPES {
  courier = "courier",
  mail = "mail",
  pickup = "pickup"
}

export enum DELIVERY_STATUS {
  pending = "pending",
  inProgress = "inProgress",
  confirm = "confirm",
  shipped = "shipped",
  done = "done"
}

export enum PAYMENT_TYPES {
  cash = "cash",
  card = "card"
}
export enum PAYMENT_STATUS {
  pending = "pending",
  paid = "paid",
  cancel = "cancel"
}
