import { differenceBy, isEmpty, set } from "lodash";
import { Inject, Service } from "typedi";
import { In, Raw, Repository } from "typeorm";
import { InjectRepository } from "typeorm-typedi-extensions";
import {
  DosageInput,
  ProductAddInput,
  ProductsWhereInput,
  ProductUpdateInput,
  ProductWhereInput
} from "../dto";
import { ProductEntity } from "../entities";
import { DosagesService } from "./dosages-service";

@Service()
export class ProductsService {
  @InjectRepository(ProductEntity)
  private repository: Repository<ProductEntity>;

  @Inject()
  private dosagesService: DosagesService;

  find ({
    id,
    title,
    url,
    ...where
  }: ProductsWhereInput & {
    skip?: number;
    take?: number;
  }) {
    const _where = { ...where };

    if (id) {
      set(_where, "id", In(id));
    }

    if (title) {
      set(_where, "title", Raw(alias => `${alias} ILike '${title}%'`));
    }

    if (url) {
      set(_where, "url", Raw(alias => `${alias} ILike '${url}%'`));
    }

    return this.repository.findAndCount({ where: _where });
  }

  findOne (where: ProductWhereInput) {
    return this.repository.findOne({ where });
  }

  async create ({ dosages, ...dto }: ProductAddInput) {
    const { id } = await this.repository.create(dto).save();

    await this.setDosages(id, dosages);

    return this.findOne({ id });
  }

  async update ({ id, dosages, ...dto }: ProductUpdateInput) {
    await this.repository.update({ id }, dto);
    await this.setDosages(id, dosages);

    return this.findOne({ id });
  }

  remove (where: ProductWhereInput) {
    return this.repository.delete(where);
  }

  async setDosages (productId: number, dosages: DosageInput[]) {
    if (isEmpty(dosages)) return false;

    const existed = await this.dosagesService.find({ productId });
    const toDeleteIds = differenceBy(existed, dosages, "id").map(d => d.id);

    if (!isEmpty(toDeleteIds)) {
      await this.dosagesService.remove({ id: toDeleteIds });
    }

    return Promise.all(
      dosages.map(async d => {
        if (d.id) {
          await this.dosagesService.update({
            ...d,
            productId
          });

          return this.dosagesService.findOne({ id: d.id });
        } else {
          const { id } = await this.dosagesService.create({
            ...d,
            productId
          });

          return this.dosagesService.findOne({ id });
        }
      })
    );
  }
}
