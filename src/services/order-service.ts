import { differenceBy, isEmpty, set } from "lodash";
import { Service } from "typedi";
import { In, Like, Raw, Repository } from "typeorm";
import { InjectRepository } from "typeorm-typedi-extensions";
import {
  CartInput,
  OrderAddInput,
  OrdersWhereInput,
  OrderUpdateInput,
  OrderWhereInput
} from "../dto";
import { CartEntity } from "../entities/cart.entity";
import { OrderEntity } from "../entities/order.entity";

@Service()
export class OrderService {
  @InjectRepository(OrderEntity)
  private readonly repository: Repository<OrderEntity>;

  @InjectRepository(CartEntity)
  private readonly cartRepository: Repository<CartEntity>;

  find ({
    number: n,
    phone,
    customerName,
    ...where
  }: OrdersWhereInput & { skip?: number; take?: number }) {
    const _where = { ...where };

    if (n) {
      set(_where, "number", Raw(alias => `${alias} ILike '${n}%'`));
    }

    if (customerName) {
      set(
        _where,
        "customerName",
        Raw(alias => `${alias} ILike '${customerName}%'`)
      );
    }

    if (phone) {
      set(_where, "phone", Like(`${phone}%`));
    }

    // TODO: add order
    return this.repository.findAndCount({
      where: _where
    });
  }

  findOne (where: OrderWhereInput) {
    return this.repository.findOne({ where });
  }

  async create ({ cart, ...dto }: OrderAddInput) {
    const { id } = await this.repository
      .create({
        ...dto,
        number: await this.genOrderNumber()
      })
      .save();

    await this.setCart(id, cart);

    return this.findOne({ id });
  }

  async update ({ id, cart, ...dto }: OrderUpdateInput) {
    await this.repository.update({ id }, dto);

    await this.setCart(id, cart);

    return this.findOne({ id });
  }

  async setCart (orderId: number, cart: CartInput[]) {
    const existed = await this.cartRepository.find({ orderId });
    const toDeleteIds = differenceBy(existed, cart, "id").map(d => d.id);

    if (!isEmpty(toDeleteIds)) {
      await this.cartRepository.delete({ id: In(toDeleteIds) });
    }

    return Promise.all(
      cart.map(async c => {
        if (c.id) {
          await this.cartRepository.update(
            { id: c.id },
            {
              ...c,
              orderId
            }
          );

          return this.cartRepository.findOne({ id: c.id });
        } else {
          const { id } = await this.cartRepository
            .create({
              ...c,
              orderId
            })
            .save();

          return this.cartRepository.findOne({ id });
        }
      })
    );
  }

  // TODO: make 5 symbols in string
  async genOrderNumber () {
    const draftNumber = `${Date.now()
      .toString(36)
      .substr(0, 3)}${Math.random()
      .toString(36)
      .substr(2, 6)}`;

    const ORDER_NUMBER_LENGTH = 5;
    const orderNumber = draftNumber.substr(2, ORDER_NUMBER_LENGTH);
    const order = await this.findOne({
      number: orderNumber
    });

    if (!order) {
      return orderNumber;
    } else {
      return this.genOrderNumber();
    }
  }
}
