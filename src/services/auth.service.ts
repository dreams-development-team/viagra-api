import { sign, verify } from "jsonwebtoken";
import { Service } from "typedi";
import { JWT_SECRET } from "../config";

@Service()
export class AuthService {
  createToken (id: number) {
    return sign({ id }, JWT_SECRET, { expiresIn: "7d" });
  }

  verifyToken (token: string) {
    return verify(token, JWT_SECRET) as { id: number };
  }
}
