import { differenceBy, isArray, isEmpty } from "lodash";
import { Service } from "typedi";
import { In, Repository } from "typeorm";
import { InjectRepository } from "typeorm-typedi-extensions";
import { DosageDetailInput, DosageInput } from "../dto";
import { DosageDetailsEntity, DosageEntity } from "../entities";

@Service()
export class DosagesService {
  @InjectRepository(DosageEntity)
  private repository: Repository<DosageEntity>;

  find ({ productId }: { productId: number }) {
    return this.repository.find({ where: { productId } });
  }

  findOne (where: { id: number }) {
    return this.repository.findOne(where);
  }

  async create ({ details, ...dto }: DosageInput) {
    const { id } = await this.repository.create(dto).save();

    await this.setDetails(id, details);

    return this.findOne({ id });
  }

  async update ({ id, details, ...dto }: DosageInput) {
    await this.repository.update({ id }, dto);

    await this.setDetails(id, details);

    return this.findOne({ id });
  }

  remove ({ id }: { id: number | number[] }) {
    return this.repository.delete({
      id: isArray(id) ? In(id) : id
    });
  }

  async setDetails (dosageId: number, details: DosageDetailInput[]) {
    if (isEmpty(details)) return false;

    const existed = await DosageDetailsEntity.find({ dosageId });
    const toDeleteIds = differenceBy(existed, details, "id").map(d => d.id);

    if (!isEmpty(toDeleteIds)) {
      await DosageDetailsEntity.delete({ id: In(toDeleteIds) });
    }

    return Promise.all(
      details.map(async d => {
        if (d.id) {
          await DosageDetailsEntity.update(
            { id: d.id },
            {
              ...d,
              dosageId
            }
          );

          return DosageDetailsEntity.findOne({ id: d.id });
        } else {
          const { id } = await DosageDetailsEntity.create({
            ...d,
            dosageId
          }).save();

          return DosageDetailsEntity.findOne({ id });
        }
      })
    );
  }
}
