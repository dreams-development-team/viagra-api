import { Field, InputType, ObjectType, registerEnumType } from "type-graphql";
import {
  DELIVERY_STATUS,
  DELIVERY_TYPES,
  PAYMENT_STATUS,
  PAYMENT_TYPES
} from "./constants";

@InputType("FileInput")
@ObjectType()
export class File {
  @Field({ nullable: true })
  uid?: string;

  @Field({ nullable: true })
  name?: string;

  @Field()
  url: string;
}

registerEnumType(DELIVERY_STATUS, {
  name: "DeliveryStatus",
  description: "Order delivery status"
});
registerEnumType(DELIVERY_TYPES, {
  name: "DeliveryTypes",
  description: "Order delivery types"
});

registerEnumType(PAYMENT_STATUS, {
  name: "PaymentStatus",
  description: "Order payment status"
});
registerEnumType(PAYMENT_TYPES, {
  name: "PaymentTypes",
  description: "Order payment types"
});
