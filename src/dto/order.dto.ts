import { Field, InputType, ObjectType } from "type-graphql";
import {
  DELIVERY_STATUS,
  DELIVERY_TYPES,
  PAYMENT_STATUS,
  PAYMENT_TYPES
} from "../constants";
import { OrderEntity } from "../entities/order.entity";

@InputType("AddressInput")
@ObjectType()
export class OrderAddress {
  @Field()
  city: string;

  @Field()
  street: string;

  @Field()
  apartment: string;

  @Field()
  postalCode: string;
}

@InputType()
export class CartInput {
  @Field({ nullable: true })
  id?: number;

  @Field()
  quantity: number;

  @Field()
  dosageId: number;

  @Field()
  productId: number;

  @Field()
  dosageDetailId: number;
}

@InputType()
export class OrderAddInput {
  @Field(() => DELIVERY_TYPES)
  deliveryType: string;

  @Field(() => DELIVERY_STATUS)
  deliveryStatus: string;

  @Field(() => PAYMENT_TYPES)
  paymentType: string;

  @Field(() => PAYMENT_STATUS)
  paymentStatus: string;

  @Field()
  customerName: string;

  @Field({ nullable: true })
  email?: string;

  @Field(() => OrderAddress)
  address: OrderAddress;

  @Field({ nullable: true })
  phone?: string;

  @Field({ nullable: true })
  comment?: string;

  @Field(() => [CartInput])
  cart: CartInput[];
}

@InputType()
export class OrderUpdateInput {
  @Field()
  id: number;

  @Field(() => DELIVERY_TYPES, { nullable: true })
  deliveryType?: string;

  @Field(() => DELIVERY_STATUS, { nullable: true })
  deliveryStatus?: string;

  @Field(() => PAYMENT_TYPES, { nullable: true })
  paymentType?: string;

  @Field(() => PAYMENT_STATUS, { nullable: true })
  paymentStatus?: string;

  @Field({ nullable: true })
  customerName?: string;

  @Field({ nullable: true })
  email?: string;

  @Field(() => OrderAddress, { nullable: true })
  address?: OrderAddress;

  @Field({ nullable: true })
  phone?: string;

  @Field({ nullable: true })
  comment?: string;

  @Field(() => [CartInput], { nullable: true })
  cart?: CartInput[];
}

@InputType()
export class OrdersWhereInput {
  @Field({ nullable: true })
  deliveryType?: string;

  @Field({ nullable: true })
  paymentType?: string;

  @Field({ nullable: true })
  deliveryStatus?: string;

  @Field({ nullable: true })
  paymentStatus?: string;

  @Field({ nullable: true })
  customerName?: string;

  @Field({ nullable: true })
  phone?: string;

  @Field({ nullable: true })
  number?: string;
}

@InputType()
export class OrderWhereInput {
  @Field({ nullable: true })
  id?: number;

  @Field({ nullable: true })
  number?: string;
}

@ObjectType()
export class OrdersPayload {
  @Field(() => [OrderEntity])
  data: OrderEntity[];

  @Field()
  count: number;
}
