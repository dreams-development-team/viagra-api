import { Field, InputType, ObjectType } from "type-graphql";
import { ProductEntity } from "../entities";
import { File } from "../graphql-types";

@InputType()
export class DosageDetailInput {
  @Field({ nullable: true })
  id?: number;

  @Field()
  count: number;

  @Field()
  price: number;

  @Field({ nullable: true })
  dosageId?: number;
}

@InputType("ProductMetaInput")
@ObjectType()
export class ProductMeta {
  @Field()
  key: string;

  @Field()
  value: string;
}

@InputType()
export class DosageInput {
  @Field({ nullable: true })
  id?: number;

  @Field()
  dosage: string;

  @Field({ nullable: true })
  productId?: number;

  @Field(() => [DosageDetailInput])
  details: DosageDetailInput[];
}

@InputType()
export class ProductAddInput {
  @Field()
  title: string;

  @Field()
  url: string;

  @Field(() => File)
  logo: object;

  @Field(() => [File], { nullable: true })
  gallery?: object[];

  @Field()
  withAlcohol: boolean;

  @Field()
  timeToStart: string;

  @Field()
  workTime: string;

  @Field(() => [ProductMeta], { nullable: true })
  meta: ProductMeta[];

  @Field(() => [DosageInput])
  dosages: DosageInput[];

  @Field()
  description: string;

  @Field()
  seoTitle: string;

  @Field()
  seoDescription: string;

  @Field(() => [String])
  seoKeywords?: string[];
}

@InputType()
export class ProductUpdateInput {
  @Field()
  id: number;

  @Field({ nullable: true })
  title?: string;

  @Field({ nullable: true })
  url?: string;

  @Field(() => File, { nullable: true })
  logo?: object;

  @Field(() => [File], { nullable: true })
  gallery?: object[];

  @Field({ nullable: true })
  withAlcohol?: boolean;

  @Field({ nullable: true })
  timeToStart?: string;

  @Field({ nullable: true })
  workTime?: string;

  @Field(() => [ProductMeta], { nullable: true })
  meta?: ProductMeta[];

  @Field(() => [DosageInput], { nullable: true })
  dosages?: DosageInput[];

  @Field({ nullable: true })
  description?: string;

  @Field({ nullable: true })
  seoTitle?: string;

  @Field({ nullable: true })
  seoDescription?: string;

  @Field(() => [String], { nullable: true })
  seoKeywords?: string[];
}

@InputType()
export class ProductWhereInput {
  @Field({ nullable: true })
  id?: number;

  @Field({ nullable: true })
  title?: string;

  @Field({ nullable: true })
  url?: string;
}

@InputType()
export class ProductsWhereInput {
  @Field(() => [Number], { nullable: true })
  id?: number[];

  @Field({ nullable: true })
  title?: string;

  @Field({ nullable: true })
  url?: string;
}

@ObjectType()
export class ProductsPayload {
  @Field(() => [ProductEntity])
  data: ProductEntity[];

  @Field()
  count: number;
}
