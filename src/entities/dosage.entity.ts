import { Field, ObjectType } from "type-graphql";
import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn
} from "typeorm";
import { DosageDetailsEntity } from "./dosage-details.entity";
import { ProductEntity } from "./product.entity";

@ObjectType()
@Entity("dosages")
export class DosageEntity extends BaseEntity {
  @Field()
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column()
  dosage: string;

  @Field()
  @Column()
  productId: number;

  @ManyToOne(() => ProductEntity, p => p.dosages, {
    onDelete: "CASCADE"
  })
  @JoinColumn({ name: "productId" })
  product: ProductEntity;

  @Field(() => [DosageDetailsEntity])
  @OneToMany(() => DosageDetailsEntity, dd => dd.dosage, {
    eager: true
  })
  details: DosageDetailsEntity[];
}
