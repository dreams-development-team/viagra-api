import { Field, ObjectType } from "type-graphql";
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";
import {
  DELIVERY_STATUS,
  DELIVERY_TYPES,
  PAYMENT_STATUS,
  PAYMENT_TYPES
} from "../constants";
import { OrderAddress } from "../dto";
import { CartEntity } from "./cart.entity";

@ObjectType()
@Entity("orders")
export class OrderEntity extends BaseEntity {
  @Field()
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column({ unique: true })
  number: string;

  @Field(() => DELIVERY_TYPES)
  @Column({ type: "enum", enum: DELIVERY_TYPES })
  deliveryType: string;

  @Field(() => DELIVERY_STATUS)
  @Column({ type: "enum", enum: DELIVERY_STATUS })
  deliveryStatus: string;

  @Field(() => PAYMENT_TYPES)
  @Column({ type: "enum", enum: PAYMENT_TYPES })
  paymentType: string;

  @Field(() => PAYMENT_STATUS)
  @Column({ type: "enum", enum: PAYMENT_STATUS })
  paymentStatus: string;

  @Field()
  @Column()
  customerName: string;

  @Field({ nullable: true })
  @Column({ nullable: true })
  email?: string;

  @Field(() => OrderAddress)
  @Column({ type: "jsonb" })
  address: OrderAddress;

  @Field({ nullable: true })
  @Column({ nullable: true })
  phone?: string;

  @Field({ nullable: true })
  @Column({ nullable: true })
  comment?: string;

  @Field(() => [CartEntity])
  @OneToMany(() => CartEntity, c => c.order, { eager: true })
  cart: CartEntity[];

  @Field()
  @CreateDateColumn()
  createdAt: string;

  @Field()
  @UpdateDateColumn()
  updatedAt: string;
}
