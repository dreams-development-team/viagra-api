import { Field, ObjectType } from "type-graphql";
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";
import { ProductMeta } from "../dto";
import { File } from "../graphql-types";
import { DosageEntity } from "./dosage.entity";

@ObjectType()
@Entity("products")
export class ProductEntity extends BaseEntity {
  @Field()
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column({ unique: true })
  title: string;

  @Field()
  @Column({ unique: true })
  url: string;

  @Field()
  @Column({ default: false })
  withAlcohol: boolean;

  @Field()
  @Column()
  timeToStart: string;

  @Field()
  @Column()
  workTime: string;

  @Field(() => File)
  @Column({ type: "jsonb" })
  logo: File;

  @Field(() => [File], { nullable: true })
  @Column({ type: "jsonb", default: [] })
  gallery: File[];

  @Field(() => [ProductMeta], { nullable: true })
  @Column({ type: "jsonb", default: [] })
  meta: ProductMeta[];

  @Field()
  @Column()
  description: string;

  @Field()
  @Column()
  seoTitle: string;

  @Field()
  @Column()
  seoDescription: string;

  @Field(() => [String])
  @Column({ type: "jsonb", default: [] })
  seoKeywords: string[];

  @Field(() => [DosageEntity])
  @OneToMany(() => DosageEntity, d => d.product, { eager: true })
  dosages: DosageEntity[];

  @Field()
  @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
  createdAt: string;

  @Field()
  @UpdateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
  updatedAt: string;
}
