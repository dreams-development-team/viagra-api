import { Field, ObjectType } from "type-graphql";
import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn
} from "typeorm";
import { DosageEntity } from "./dosage.entity";

@ObjectType()
@Entity("dosages_details")
export class DosageDetailsEntity extends BaseEntity {
  @Field()
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column()
  count: number;

  @Field()
  @Column("decimal", { precision: 5, scale: 2 })
  price: number;

  @Field()
  @Column()
  dosageId: number;

  @ManyToOne(() => DosageEntity, d => d.details, {
    onDelete: "CASCADE"
  })
  @JoinColumn({ name: "dosageId" })
  dosage: DosageEntity;
}
