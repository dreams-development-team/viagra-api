import { Field, ObjectType } from "type-graphql";
import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn
} from "typeorm";
import { DosageDetailsEntity } from "./dosage-details.entity";
import { DosageEntity } from "./dosage.entity";
import { OrderEntity } from "./order.entity";
import { ProductEntity } from "./product.entity";

@ObjectType()
@Entity("carts")
export class CartEntity extends BaseEntity {
  @Field()
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column()
  quantity: number;

  @Field()
  @Column()
  orderId: number;

  @Field()
  @Column()
  productId: number;

  @Field()
  @Column()
  dosageId: number;

  @Field()
  @Column()
  dosageDetailId: number;

  @Field(() => OrderEntity)
  @ManyToOne(() => OrderEntity, o => o.cart, { onDelete: "CASCADE" })
  @JoinColumn({ name: "orderId" })
  order: OrderEntity;

  @Field(() => ProductEntity)
  @ManyToOne(() => ProductEntity, {
    eager: true,
    onDelete: "CASCADE"
  })
  @JoinColumn({ name: "productId" })
  product: ProductEntity;

  @Field(() => DosageEntity)
  @ManyToOne(() => DosageEntity, { eager: true })
  dosage: DosageEntity;

  @Field(() => DosageDetailsEntity)
  @ManyToOne(() => DosageDetailsEntity, { eager: true })
  dosageDetail: DosageDetailsEntity;
}
