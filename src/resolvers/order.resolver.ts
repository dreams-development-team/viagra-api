import { Arg, Authorized, Mutation, Query, Resolver } from "type-graphql";
import { Inject, Service } from "typedi";
import {
  OrderAddInput,
  OrdersPayload,
  OrdersWhereInput,
  OrderUpdateInput,
  OrderWhereInput
} from "../dto";
import { OrderEntity } from "../entities/order.entity";
import { OrderService } from "../services/order-service";

@Service()
@Resolver()
export class OrderResolver {
  @Inject()
  private readonly ordersService: OrderService;

  @Mutation(() => OrderEntity)
  async createOrder (@Arg("data") data: OrderAddInput): Promise<OrderEntity> {
    // TODO: avoid to create with paymentType for not auth
    return this.ordersService.create(data);
  }

  @Authorized()
  @Mutation(() => OrderEntity)
  async updateOrder (@Arg("data") data: OrderUpdateInput): Promise<OrderEntity> {
    return this.ordersService.update(data);
  }

  @Authorized()
  @Query(() => OrdersPayload)
  async searchOrders (
    @Arg("where", { nullable: true, defaultValue: {} })
    where: OrdersWhereInput,
    @Arg("skip", { nullable: true, defaultValue: 0 }) skip: number,
    @Arg("take", { nullable: true, defaultValue: 20 }) take: number
  ): Promise<OrdersPayload> {
    const [data, count] = await this.ordersService.find({
      ...where,
      skip,
      take
    });

    return {
      data,
      count
    };
  }

  @Authorized()
  @Query(() => OrderEntity, { nullable: true })
  async order (
    @Arg("where") where: OrderWhereInput
  ): Promise<OrderEntity | null> {
    return this.ordersService.findOne(where);
  }
}
