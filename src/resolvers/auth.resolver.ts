import {
  Arg,
  Ctx,
  Field,
  Mutation,
  ObjectType,
  Query,
  Resolver
} from "type-graphql";
import { Inject, Service } from "typedi";
import { UserEntity } from "../entities";
import { Context } from "../main";
import { AuthService, UsersService } from "../services";

@ObjectType()
export class Token {
  @Field()
  token: string;
}

@Service()
@Resolver()
export class AuthResolver {
  @Inject()
  private readonly usersService: UsersService;

  @Inject()
  private readonly authService: AuthService;

  @Mutation(() => Token, { nullable: true })
  async signIn (
    @Arg("login") login: string,
    @Arg("password") password: string
  ): Promise<Token | null> {
    const user = await this.usersService.findOne({ login });

    if (!user) {
      return null;
    }

    if (!this.usersService.comparePassword(password, user.passwordHash)) {
      // TODO: throw apollo error
      return null;
    }

    return {
      token: this.authService.createToken(user.id)
    };
  }

  @Query(() => UserEntity, { nullable: true })
  authUser (@Ctx() { state: { user } }: Context) {
    // TODO: remove nullable, throw apollo error
    return user;
  }
}
