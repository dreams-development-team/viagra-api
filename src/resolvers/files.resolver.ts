import cloudinary from "cloudinary";
import { GraphQLUpload } from "graphql-upload";
import { Stream } from "stream";
import { Arg, Authorized, Mutation, Resolver } from "type-graphql";
import { CLOUD_API_KEY, CLOUD_NAME, CLOUD_SECRET } from "../config";

cloudinary.config({
  cloud_name: CLOUD_NAME,
  api_key: CLOUD_API_KEY,
  api_secret: CLOUD_SECRET
});

interface File {
  createReadStream: () => Stream;
  filename: string;
  mimetype: string;
  encoding: string;
}

@Resolver()
export class FilesResolver {
  @Authorized()
  @Mutation(() => String)
  async upload (@Arg("file", () => GraphQLUpload)
  {
    createReadStream
  }: File) {
    return new Promise((res, rej) => {
      const uploadStream = cloudinary.v2.uploader.upload_stream((e, r) => {
        if (e) {
          rej(JSON.stringify(e));
        } else {
          res(r.url);
        }
      });

      createReadStream().pipe(uploadStream);
    });
  }
}
