import { Arg, Authorized, Mutation, Query, Resolver } from "type-graphql";
import { Inject, Service } from "typedi";
import {
  ProductAddInput,
  ProductsPayload,
  ProductsWhereInput,
  ProductUpdateInput,
  ProductWhereInput
} from "../dto";
import { ProductEntity } from "../entities";
import { ProductsService } from "../services";

@Service()
@Resolver()
export class ProductResolver {
  @Inject()
  private productsService: ProductsService;

  @Authorized()
  @Mutation(() => ProductEntity)
  async createProduct (
    @Arg("data") data: ProductAddInput
  ): Promise<ProductEntity> {
    return this.productsService.create(data);
  }

  @Authorized()
  @Mutation(() => ProductEntity)
  async updateProduct (
    @Arg("data") data: ProductUpdateInput
  ): Promise<ProductEntity> {
    return this.productsService.update(data);
  }

  @Query(() => ProductEntity, { nullable: true })
  async product (
    @Arg("where") where: ProductWhereInput
  ): Promise<ProductEntity | null> {
    return this.productsService.findOne(where);
  }

  @Authorized()
  @Mutation(() => Boolean)
  async removeProduct (
    @Arg("where") where: ProductWhereInput
  ): Promise<boolean> {
    const { affected } = await this.productsService.remove(where);

    return Boolean(affected);
  }

  @Query(() => ProductsPayload)
  async searchProducts (
    @Arg("where", { nullable: true, defaultValue: {} })
    where: ProductsWhereInput,
    @Arg("skip", { nullable: true, defaultValue: 0 }) skip: number,
    @Arg("take", { nullable: true, defaultValue: 20 }) take: number
  ): Promise<ProductsPayload> {
    const [data, count] = await this.productsService.find({
      ...where,
      skip,
      take
    });

    return {
      data,
      count
    };
  }
}
