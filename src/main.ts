import { ApolloServer, AuthenticationError } from "apollo-server-express";
import express from "express";
import "reflect-metadata";
import { AuthChecker, buildSchema } from "type-graphql";
import { Container } from "typedi";
import { useContainer as useTypeOrmContainer } from "typeorm";
import { HOST, IS_PROD, PORT } from "./config";
import { dbConnect } from "./db";
import { UserEntity } from "./entities";
import { AuthService, UsersService } from "./services";

export type Context = {
  req: Request;
  state: {
    user?: UserEntity;
  };
};

const authChecker: AuthChecker<Context> = ({
  context: {
    state: { user }
  }
}) => Boolean(user);

useTypeOrmContainer(Container);

export const bootstrap = async () => {
  await dbConnect();

  const app = express();

  const authService = Container.get(AuthService);
  const userService = Container.get(UsersService);

  const server = new ApolloServer({
    schema: await buildSchema({
      container: Container,
      resolvers: [__dirname + "/resolvers/*.ts"],
      authChecker
    }),
    context: async ({ req }) => {
      let user;

      if (req.headers.authorization) {
        try {
          const { id } = authService.verifyToken(
            req.headers.authorization.split(" ")[1]
          );

          user = await userService.findOne({ id });
        } catch {
          throw new AuthenticationError("JWT Error");
        }
      }

      return {
        req,
        state: {
          user
        }
      };
    },
    playground: !IS_PROD
  });

  server.applyMiddleware({ app });

  app.listen(PORT, HOST, () => {
    console.info(`Server running on: "${HOST}:${PORT}"`);
  });
};
